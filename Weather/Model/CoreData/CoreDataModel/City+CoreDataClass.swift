//
//  City+CoreDataClass.swift
//  
//
//  Created by Igor Nikolaev on 23/08/2018.
//
//

import Foundation
import CoreData

@objc(City)
public class City: NSManagedObject {
    
    convenience init(coreDataManager: CoreDataManagerProtocol) {
        self.init(entity: coreDataManager.entityForName(entityName: "City"), insertInto:coreDataManager.persistentContainer.viewContext )
    }

}
