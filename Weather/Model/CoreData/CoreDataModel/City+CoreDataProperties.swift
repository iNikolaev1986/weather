//
//  City+CoreDataProperties.swift
//  
//
//  Created by Igor Nikolaev on 23/08/2018.
//
//

import Foundation
import CoreData


extension City {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<City> {
        return NSFetchRequest<City>(entityName: "City")
    }

    @NSManaged public var name: String?

}
