//
//  WeatherWeekResponse.swift
//  Weather
//
//  Created by Igor Nikolaev on 26/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Foundation

struct WeatherWeekResponse: Decodable {
    var cod: String?
    var message: Double?
    var city: CityResponse?
    var cnt: Int?
    var list: [List]?
}

struct CityResponse: Decodable {
    var name: String?
    var country: String?
    var population: Int?
    var id: Int?
    var coord: Coord?
}

struct Coord: Decodable {
    var lon: Double?
    var lat: Double?
}

struct List: Decodable {
    var dt: Int?
    var weather: [Weather]?
    var clouds: [String: Int]?
    var main: [String: Double]?
    var wind: [String: Double]?
    var rain: [String: Double]?
    var sys: [String: String]?
    var dt_txt: String?
}

struct Weather: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}


