//
//  WeatherService.swift
//  Weather
//
//  Created by Igor Nikolaev on 21/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Moya

enum WeatherService {
    case getWeekWeather(nameCity: String)
}

extension WeatherService: TargetType {
    
    var baseURL: URL {
        return URL(string: "http://api.openweathermap.org/data/2.5")!
    }
    
    var path: String {
        switch self {
        case .getWeekWeather:
            return "forecast"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        guard let parameters = parameters else {
            return .requestPlain
        }
        return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getWeekWeather(let nameCity):
            return ["q": nameCity,
                    "appid": "d3f8c7be539e754ca5039217ef07d966",
                    "cnt": "5"]
            //http://api.openweathermap.org/data/2.5/forecast?appid=d3f8c7be539e754ca5039217ef07d966&cnt=5&q=Moscow
            //http://api.openweathermap.org/data/2.5/forecast?appid=5c53d3bab088e565436b31dd5ffb990a&cnt=5&q=Moscow
        }
    }
    
    
}


