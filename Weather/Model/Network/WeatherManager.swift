//
//  WeatherManager.swift
//  Weather
//
//  Created by Igor Nikolaev on 21/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Moya

protocol WeatherManagerProtocol {
    func requestWeek(nameCity: String , comletion: @escaping (WeatherWeekResponse) -> Void)
}

class WeatherManager:  WeatherManagerProtocol {
   
    private let provider = MoyaProvider<WeatherService>()
    
   func requestWeek(nameCity: String, comletion: @escaping (WeatherWeekResponse) -> Void) {
//                if let request = try? self.provider.endpoint(WeatherService.getWeekWeather(nameCity: nameCity)).urlRequest(){
//                    let url = request.url
//                    let key = url?.absoluteString
//                    print(key!)
//                }
        provider.request(.getWeekWeather(nameCity: nameCity)) { result in
            switch result {
            case .success(let response):
                if let weatherWeekResponse: WeatherWeekResponse = try? response.map(WeatherWeekResponse.self) {
                    DispatchQueue.main.async {
                      comletion(weatherWeekResponse)
                    }
                }
            case .failure(let error):
                print(error.errorDescription ?? "Unknown error")
            }
        }
    }
    
}
