//
//  DayViewCell.swift
//  Weather
//
//  Created by Igor Nikolaev on 22/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import UIKit

class DayViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
