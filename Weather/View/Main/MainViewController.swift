//
//  ViewController.swift
//  Weather
//
//  Created by Igor Nikolaev on 19/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, SlideDelegate, UIScrollViewDelegate {
    //привет
    //Hello
    @IBOutlet weak var scrollView: UIScrollView!
    
    var slides: [Slide] = []
    var viewModel: MainViewModelProtocol! {
        didSet {
            viewModel.updateWeather() {
                self.slides = self.createSlides()
                self.setupSlideScrollViewWith(slides: self.slides)
            }
        }
    }
    
    //MARK: - life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = MainViewModel(weatherManager: WeatherManager(),coreDataManager: CoreDataManager())
        self.scrollView.delegate = self
    }
    
    //MARK: - create Slides
    
    func createSlideWith(slideViewModel: SlideViewModel ) -> Slide {
        let slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
        slide.delegate = self
        slide.viewModel = slideViewModel
        slide.tableView.register(UINib(nibName: "DayViewCell", bundle: nil), forCellReuseIdentifier: "dayCell")
        return slide
    }
    
    func createSlides() -> [Slide] {
        var generateSlides = [Slide]()
        for slideViewModel in viewModel.slidesViewModelArray {
            let slide =  createSlideWith(slideViewModel: slideViewModel)
            generateSlides.append(slide)
        }
        return generateSlides
    }
    
    func setupSlideScrollViewWith(slides: [Slide]) {
        let numberOfSlides = slides.count
        scrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(numberOfSlides), height: view.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0..<numberOfSlides {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.addSubview(slides[i])
        }
    }
    
    //MARK: - Alert
    
    func add() {
        let alert = UIAlertController(title: "City", message: "Enter name city", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        let add = UIAlertAction(title: "add", style: .default) { (action) -> Void in
            if let textField = alert.textFields?.first {
                self.addSlide(city: textField.text!)
            }
        }
        alert.addAction(add)
        alert.addTextField { (textField) -> Void in
            textField.placeholder = "Enter city name"
        }
        present(alert, animated: true, completion: nil)
    }
    
    func addSlide(city: String) {
        self.viewModel.save(city: city) {[weak self] in
            let slide = self?.createSlideWith(slideViewModel: (self?.viewModel.slidesViewModelArray.last!)!)
            self?.slides.append(slide!)
            self?.setupSlideScrollViewWith(slides: (self?.slides)!)
        }
    }
    
    func delete() {
        let page = getCurrentPage()
        if slides.count > 1 {
            viewModel.delete(city: viewModel.slidesViewModelArray[page].name)
            slides.remove(at: page)
            setupSlideScrollViewWith(slides: slides)
            scrollView.setNeedsDisplay()
        }
    }
    
    func getCurrentPage() -> Int {
        let index  = round(scrollView.contentOffset.x/scrollView.frame.width)
        let page = Int(index)
        return page
    }
    
}
