//
//  Slide.swift
//  Weather
//
//  Created by Igor Nikolaev on 22/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import UIKit

protocol SlideDelegate: class {
    func add()
    func delete()
}

class Slide: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tempLabel: UILabel!
    
    var viewModel: SlideViewModelProtocol! {
        didSet {
            self.label.text = viewModel.name
            self.tempLabel.text = viewModel.tempForWeek[0]
            self.tableView.reloadData()
            tableView.register(UINib(nibName: "DayViewCell", bundle: nil), forCellReuseIdentifier: "dayCell")
            self.tableView.dataSource = self
        }
    }
    
   weak var delegate: SlideDelegate?
    
    @IBAction func addCity(_ sender: Any) {
        delegate?.add()
    }
    
    @IBAction func deleteCity(_ sender: Any) {
        delegate?.delete()
    }
}

extension Slide : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath) as! DayViewCell
        cell.dayLabel.text = viewModel.currentWeek[indexPath.row]
        cell.tempLabel.text = viewModel.tempForWeek[indexPath.row]
        return cell
    }
    
}




