//
//  MainViewModel.swift
//  Weather
//
//  Created by Igor Nikolaev on 21/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Foundation

protocol MainViewModelProtocol {
    var slidesViewModelArray: [SlideViewModel] { get }
    
    func updateWeather(completion:@escaping () -> Void) -> Void
    func save(city: String, completion:@escaping () -> Void )
    func delete(city: String)
}

class  MainViewModel : MainViewModelProtocol {
    
    private let weatherManager: WeatherManagerProtocol
    private var citiesWeatherResposnseArray = [WeatherWeekResponse]()
    private let coreDataManager: CoreDataManagerProtocol
    
    var slidesViewModelArray = [SlideViewModel]()
    var cities = [String]()
    
    required init(weatherManager: WeatherManagerProtocol, coreDataManager: CoreDataManagerProtocol) {
        self.weatherManager = weatherManager
        self.coreDataManager = coreDataManager
    }
    
    //MARK: - Network
    
    func updateWeather(completion:@escaping () -> Void) -> Void {
        cities = self.getAllCity()
        for city in cities {
            self.getTempWeekFor(city: city, completion: completion)
        }
    }
    
     private func getTempWeekFor(city: String , completion: @escaping () -> Void) {
        weatherManager.requestWeek(nameCity: city) {[weak self] weatherWeekResponse in
            self?.citiesWeatherResposnseArray.append(weatherWeekResponse)
            self?.slidesViewModelArray.append(SlideViewModel(weatherWeekResponse: weatherWeekResponse))
            completion()
        }
    }
    
    //MARK: - Core Data Manager
    
    private func getAllCity() -> [String] {
        guard let cities = self.coreDataManager.fetch(name: "City") else {
            fatalError("Сities are absent")
        }
        return cities
    }
    
    func delete(city: String) {
        self.coreDataManager.delete(city: city.lowercased())
    }
    
    func save(city: String, completion:@escaping ()-> Void ) {
        let managedObject = City(coreDataManager: self.coreDataManager)
        managedObject.name = city.lowercased()
        self.coreDataManager.saveContext()
        getTempWeekFor(city: city, completion: completion)
    }
    
}


