//
//  SlideViewModel.swift
//  Weather
//
//  Created by Igor Nikolaev on 27/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Foundation

protocol SlideViewModelProtocol {
    var name: String { get }
    var currentWeek: [String] { get }
    var tempForWeek: [String] { get }
}

class SlideViewModel: SlideViewModelProtocol {
    
    let name: String
    var currentWeek: [String]
    var tempForWeek = [String]()
    
    required init(weatherWeekResponse: WeatherWeekResponse) {
        for temp in weatherWeekResponse.list! {
            if let tempDay  = temp.main!["temp"] {
                let realTemp = SlideViewModel.convertTemperatureToString(country: (weatherWeekResponse.city?.country)!, temperature: tempDay)
                tempForWeek.append(realTemp)
            }
        }
        self.currentWeek = SlideViewModel.getCurrentWeekDay(range: 5)
        self.name = (weatherWeekResponse.city?.name!)!
    }
    
    //MARK: - WeekDay
    
    private static func getCurrentWeekDay(range: Int) -> [String]{
        var currentWeek = [String]()
        for i in 0..<range {
            let calendar = Calendar.current
            let date = calendar.date(byAdding: .day, value: i, to: Date())!
            currentWeek.append(date.dayOfWeek()!)
        }
        return currentWeek
    }
    
    //MARK: - Convert temp
    
    private  static func convertTemperature(country: String, temperature: Double) -> Double {
        if (country == "US") {
            //Convert to F
            return (round(((temperature - 273.15) * 1.8) + 32))
        } else {
            //Convert to C
            return (round(temperature - 273.15))
        }
    }
    
    private static func convertTemperatureToString(country: String, temperature: Double) -> String {
        let temp = self.convertTemperature(country: country, temperature: temperature)
        let tempString = String(temp)
        if (country == "US") {
            return tempString + "°F"
        } else {
            return tempString + "°C"
        }
    }
}

