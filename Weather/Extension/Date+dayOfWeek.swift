//
//  Date+dayNumberOfWeek.swift
//  Weather
//
//  Created by Igor Nikolaev on 22/08/2018.
//  Copyright © 2018 Igor Nikolaev. All rights reserved.
//

import Foundation

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}
